// Copyright 2016 Matthias H�lzl, All Rights Reserved.

#pragma once

#include "GameFramework/GameMode.h"
#include "PropertyTestGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PROPERTYTEST_API APropertyTestGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	APropertyTestGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Spawning")
	void ToggleDynamicBlueprint();

	AActor* SpawnBlueprint();

	void AddEntry(AActor* Actor);

	UFUNCTION(BlueprintCallable, Category = "Blueprint Access")
	void AddEntryToStaticActor();

	UFUNCTION(BlueprintCallable, Category = "Blueprint Access")
	void AddEntryToDynamicActor();

protected:

	UBlueprint* BlueprintToUse;
	UBlueprintGeneratedClass* BlueprintClass;

	UUserDefinedStruct* StructToUse;
	
	AActor* StaticBlueprintActor;
	AActor* DynamicBlueprintActor;
};
