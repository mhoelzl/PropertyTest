# PropertyTest

A small project that demonstrates how to access Blueprint values from C++ code
in UE4.

The solution presented in this project is rather clunky, but it's the best I
could do. I'd be happy to learn about better ways of doing this.

Enjoy!

  tc